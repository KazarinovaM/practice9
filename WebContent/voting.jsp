<%@ page import="java.util.*" %>
<html>
<body>
    <form action="Result">
        <select name="select">      
            <%
                List<String> names = 
                    (List<String>)request.getAttribute("names");
                for (String name : names) {
            %>
                    <option name="<%=name%>"><%=name%></option>
            <%
                }
            %>
        </select>
        <input name="name" type="text" value="Name">
        <input type="submit" value="Go!">
    </form>
</body>
</html>