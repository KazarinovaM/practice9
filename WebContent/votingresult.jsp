
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>	
	<table border="1">
		<c:forEach var="currentCookie" items="${cookie}">
			<tr>
			<c:if test="${currentCookie.key != 'JSESSIONID'}"> 
				<c:set var="values" value="${fn:split(currentCookie.value.value, ', ')}" />
				<td><c:out value="${currentCookie.value.name}" /></td>
				<td>${fn:length(values)}</td>				
				<td><c:out value="${fn:join(values, ', ')}" /></td>
				</c:if>
			</tr>
		</c:forEach>
	</table>
	
</body>
</html>