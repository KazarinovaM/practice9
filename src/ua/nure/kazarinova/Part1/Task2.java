package ua.nure.kazarinova.Part1;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Task2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int x = Integer.parseInt(request.getParameter("x"));
		int y = Integer.parseInt(request.getParameter("y"));
		String operation = request.getParameter("op");
		int result = 0;
		switch (operation) {
		case "+":
			result = x + y;
			break;
		case "-":
			result = x - y;
			break;
		}
		request.setAttribute("result", result);
		request.getServletContext().getRequestDispatcher("/result.jsp").forward(request, response);
	}
}
