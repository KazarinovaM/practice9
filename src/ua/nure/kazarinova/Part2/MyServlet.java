package ua.nure.kazarinova.Part2;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("MyServlet#doGet");

		ServletContext sc = getServletContext();
		Object names = sc.getAttribute("names");
		request.setAttribute("names", names);
		request.getRequestDispatcher("voting.jsp").forward(request, response);
	}
}