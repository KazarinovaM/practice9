package ua.nure.kazarinova.Part2;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyResultFilter implements Filter {
	public void destroy() {
		System.out.println("MyResultFilter#destroy");
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("MyResultFilter#doFilter");
		HttpServletRequest req = (HttpServletRequest) request;
		String type = request.getParameter("select");
		String name = request.getParameter("name");
		Cookie[] cookies = req.getCookies();

		for (Cookie cookie : cookies) {
			if (cookie.getName().equals(type)) {
				StringBuilder sb = new StringBuilder(cookie.getValue());
				sb.append(name).append(" ");
				((HttpServletResponse) response).addCookie(new Cookie(type, sb.toString()));
				break;
			}

		}

		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		System.out.println("MyResultFilter#init");
	}

}
