package ua.nure.kazarinova.Part2;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyAttributesFilter implements Filter {
	public void destroy() {
		System.out.println("MyAttributesFilter#destroy");
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("MyAttributesFilter#doFilter");
		HttpServletRequest req = (HttpServletRequest) request;
		List<String> typeNames = (List<String>) req.getServletContext().getAttribute("names");
		if (req.getCookies().length == 1) {
			for (String typeName : typeNames) {
				((HttpServletResponse) response).addCookie(new Cookie(typeName, ""));
			}
		}

		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		System.out.println("MyAttributesFilter#init");
	}

}
