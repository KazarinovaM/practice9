package ua.nure.kazarinova.Part2;

import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class MyListener implements ServletContextListener, HttpSessionListener {

	public void contextInitialized(ServletContextEvent sce) {
		ServletContext sc = sce.getServletContext();
		List<String> names = Arrays.asList(sc.getInitParameter("names").split(" "));
		System.out.println("names ==> " + names);

		sc.setAttribute("names", names);
	}

	public void contextDestroyed(ServletContextEvent sce) {

	}

	@Override
	public void sessionCreated(HttpSessionEvent sc) {
		System.out.println("MyListener#sessionCreated");

	}

	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {

	}

}
