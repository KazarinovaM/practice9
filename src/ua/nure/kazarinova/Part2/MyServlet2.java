package ua.nure.kazarinova.Part2;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class MyServlet2 extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("MyServlet2#doGet");
		ServletContext sc = getServletContext();
		Object names = sc.getAttribute("names");
		request.setAttribute("names", names);
		request.getRequestDispatcher("votingresult.jsp").forward(request, response);
	}
}